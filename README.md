# Simplified Health Overview Plugin for LibreNMS #
![alt text](screenshot.png "Screenshot")

This is a plugin which provides a simple overview of your devices' health. It's designed with Linux and Windows servers in mind.
* Multiple sorting options (CPU, memory, disk usage, events, name)
* It shows aggregate CPU usage on servers running multiple cores
* It only shows physical memory, if available
* On Linux servers, only the disk root partition (/) is shown, if available
* On Windows servers, only the C:/ partition is shown, if available
* It shows the number of serious triggered events/alerts from the last 30 days

# Installation #
* Copy the HealthOverview folder and contents to your LibreNMS plugin folder (/opt/librenms/html/plugins/)
* Enable the plugin in LibreNMS (Overview -> Plugins -> Plugin Admin)

# Configuration #
Threshold values, colors and icons can be configured by editing the array values in config.php

# Usage #
Access the plugin using the Plugins menu in LibreNMS (Overview -> Plugins -> HealthOverview)