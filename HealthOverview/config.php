<?php

$thresholdsColors = array(
    "ok" => "",
    "warning" => "#ffa000",
    "critical" => "#ff4000"
    );

$thresholdsIcons = array(
    "ok" => "",
    "warning" => "exclamation-triangle",
    "critical" => "exclamation-circle"
    );

$thresholdsStorage = array(
    "warning" => 80,
    "critical" => 90
    );

$thresholdsMemory = array(
    "warning" => 80,
    "critical" => 90
    );

$thresholdsProcessors = array(
    "warning" => 80,
    "critical" => 99
    );