<?php
require 'config.php';

/*
 * Simplified Health Overview Plugin for LibreNMS
 *
 * Copyright (c) 2017 Sindre Lindstad <sindrelindstad@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.  Please see LICENSE.txt at the top level of
 * the source code distribution for details.
 */

if(isset($_POST['order'])) {
    $order = $_POST['order'];
} else {
    $order = "storage_desc";
}

if(isset($_POST['os'])) {
    $os = $_POST['os'];
} else {
    $os = "all";
}

if(isset($_POST['troubled'])) {
    $troubledOnly = true;
} else {
    $troubledOnly = false;
}

$availableSortby = array(
    "cpu_asc" => "CPU usage (low to high)",
    "cpu_desc" => "CPU usage (high to low)",
    "storage_asc" => "Disk usage (low to high)",
    "storage_desc" => "Disk usage (high to low)",
    "events_asc" => "Events count (low to high)",
    "events_desc" => "Events count (high to low)",
    "memory_asc" => "Memory usage (low to high)",
    "memory_desc" => "Memory usage (high to low)",
    "sysname_asc" => "System name (ascending)",
    "sysname_desc" => "System name (descending)"
    );

$availableOs = array(
    "all" => "All"
    );

$query = 'SELECT os FROM devices GROUP BY os';
$osList = dbFetchRows($query, $param);

foreach($osList as $osItem) {
    $availableOs[$osItem["os"]] = ucfirst($osItem["os"]);
}

ksort($availableOs);

?>
<div class="container-fluid"><div class="row"> <div class="col-md-12"> <div class="panel panel-default panel-condensed"> <div class="panel-heading"><strong>Filters</strong> </div>
<br>
<form method="post" name="orderby" id="orderby" role="form" class="form-horizontal">
  <div class="form-group">
    <label for="os" class="col-sm-4 control-label">Operating system(s)</label>
      <div class="col-sm-4">
        <select name="os" id="os" class="form-control input-sm" onChange="">
          <?php
            foreach($availableOs as $key => $value) {
                if($os == $key) {
                    echo '<option value="'.$key.'" selected>'.$value.'</option>'."\n";
                } else {
                    echo '<option value="'.$key.'">'.$value.'</option>'."\n";
                }
            }
            ?>
        </select>
      </div> 
  </div>
  <div class="form-group">
    <label for="order" class="col-sm-4 control-label">Order by</label>
      <div class="col-sm-4">
        <select name="order" id="order" class="form-control input-sm" onChange="">
        <?php
            foreach($availableSortby as $key => $value) {
                if($order == $key) {
                    echo '<option value="'.$key.'" selected>'.$value.'</option>'."\n";
                } else {
                    echo '<option value="'.$key.'">'.$value.'</option>'."\n";
                }
            }
        ?>
        </select>
      </div> 
  </div>
  <div class="form-group">
      <div class="col-sm-offset-4 col-sm-6">
          <div class="checkbox">
              <label>
                  <input type="checkbox" name="troubled" id="troubled" <?php if($troubledOnly) { echo "checked"; } ?>> Only show troubled devices
              </label>
          </div>
      </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-6">
        <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>
</div></div></div></div>


<?php

$deviceData = array();

$query = 'SELECT device_id, hostname, sysName, os, location FROM devices';
$devices = dbFetchRows($query, $param);

foreach($devices as $device) {
    $deviceData[$device["device_id"]] = array("id" => $device["device_id"], "hostname" => $device["hostname"], "sysname" => $device["sysName"], "os" => $device["os"], "location" => $device["location"]);
}

// Add storage data
foreach($deviceData as $deviceId => $deviceValues) {
    $storageQuery = 'SELECT storage_descr, storage_perc, storage_used, storage_free FROM storage WHERE device_id = '.$deviceId;
    $deviceStorage = dbFetchRows($storageQuery, $param);

    foreach($deviceStorage as $drive) {
        if (($deviceData[$deviceId]["os"] == "windows") && (strpos($drive["storage_descr"], 'C:') !== false)) { // Windows C:\
            $deviceData[$deviceId]["storage_perc"] = $drive["storage_perc"];
            $deviceData[$deviceId]["storage_used"] = $drive["storage_used"];
            $deviceData[$deviceId]["storage_free"] = $drive["storage_free"];
        } elseif (($deviceData[$deviceId]["os"] == "linux") && ($drive["storage_descr"] == "/")) { // Linux /
            $deviceData[$deviceId]["storage_perc"] = $drive["storage_perc"];
            $deviceData[$deviceId]["storage_used"] = $drive["storage_used"];
            $deviceData[$deviceId]["storage_free"] = $drive["storage_free"];
        } elseif($drive["storage_descr"] == "/") {
            $deviceData[$deviceId]["storage_perc"] = $drive["storage_perc"];
            $deviceData[$deviceId]["storage_used"] = $drive["storage_used"];
            $deviceData[$deviceId]["storage_free"] = $drive["storage_free"];
        }
    }
}

// Add memory data
foreach($deviceData as $deviceId => $deviceValues) {
    $memoryQuery = 'SELECT mempool_descr, mempool_perc, mempool_used, mempool_free, mempool_total FROM mempools WHERE device_id = '.$deviceId;
    $memoryPool = dbFetchRows($memoryQuery, $param);

    $deviceData[$deviceId]["memory_perc"] = 0;
    $deviceData[$deviceId]["memory_used"] = 0;
    $deviceData[$deviceId]["memory_free"] = 0;
    $deviceData[$deviceId]["memory_total"] = 0;

    foreach($memoryPool as $memory) {
        if(strtolower($memory["mempool_descr"]) == "physical memory") { // Physical memory only
            $deviceData[$deviceId]["memory_perc"] = $memory["mempool_perc"];
            $deviceData[$deviceId]["memory_used"] = $memory["mempool_used"];
            $deviceData[$deviceId]["memory_free"] = $memory["mempool_free"];
            $deviceData[$deviceId]["memory_total"] = $memory["mempool_total"];
        }
    }
}

// Add cpu data
foreach($deviceData as $deviceId => $deviceValues) {
    $processorsQuery = 'SELECT processor_usage, processor_descr FROM processors WHERE device_id = '.$deviceId;
    $processors = dbFetchRows($processorsQuery, $param);

    $processorsCount = count($processors);
    $processorsAverage = 0;

    foreach($processors as $processor) {
        $processorsAverage = $processorsAverage + (int)$processor["processor_usage"];
    }

    $processorsAverage = $processorsAverage / $processorsCount;

    $deviceData[$deviceId]["processors_count"] = $processorsCount;
    $deviceData[$deviceId]["processors_perc_average"] = $processorsAverage;
}

// Add events from last 30 days
foreach($deviceData as $deviceId => $deviceValues) {
    $eventsQuery = 'SELECT * FROM eventlog WHERE severity < 3 AND datetime > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY)) AND message not like \'%recovery%\' AND message not like \'%to up%\' AND device_id = '.$deviceId;
    $events = dbFetchRows($eventsQuery, $param);

    $eventsCount = count($events);

    $deviceData[$deviceId]["events_count"] = $eventsCount;
}

// Add any open alerts
foreach($deviceData as $deviceId => $deviceValues) {
    $alertsQuery = 'SELECT rule_id, state, alerted, open, timestamp FROM alerts WHERE timestamp > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY)) AND device_id = '.$deviceId;
    $alerts = dbFetchRows($alertsQuery, $param);

    $alertsCount = count($alerts);

    $alertsOpen = 0;
    foreach($alerts as $alert) {
        if((int)$alert["state"] == 1) {
            $alertsOpen++;
        }
    }

    $deviceData[$deviceId]["alerts_count"] = $alertsCount;
    $deviceData[$deviceId]["alerts_open"] = $alertsOpen;
}

// Sort array
if($order == "cpu_asc") {
    usort($deviceData, function ($item1, $item2) {
        return $item1['processors_perc_average'] <=> $item2['processors_perc_average'];
    });
}
if($order == "cpu_desc") {
    usort($deviceData, function ($item1, $item2) {
        return $item2['processors_perc_average'] <=> $item1['processors_perc_average'];
    });
}
if($order == "memory_asc") {
    usort($deviceData, function ($item1, $item2) {
        return $item1['memory_perc'] <=> $item2['memory_perc'];
    });
}
if($order == "memory_desc") {
    usort($deviceData, function ($item1, $item2) {
        return $item2['memory_perc'] <=> $item1['memory_perc'];
    });
}
if($order == "storage_asc") {
    usort($deviceData, function ($item1, $item2) {
        return $item1['storage_perc'] <=> $item2['storage_perc'];
    });
}
if($order == "storage_desc") {
    usort($deviceData, function ($item1, $item2) {
        return $item2['storage_perc'] <=> $item1['storage_perc'];
    });
}
if($order == "sysname_asc") {
    usort($deviceData, function ($item1, $item2) {
        return $item1['sysname'] <=> $item2['sysname'];
    });
}
if($order == "sysname_desc") {
    usort($deviceData, function ($item1, $item2) {
        return $item2['sysname'] <=> $item1['sysname'];
    });
}
if($order == "events_asc") {
    usort($deviceData, function ($item1, $item2) {
        return $item1['events_count'] <=> $item2['events_count'];
    });
}
if($order == "events_desc") {
    usort($deviceData, function ($item1, $item2) {
        return $item2['events_count'] <=> $item1['events_count'];
    });
}

$table = <<<EOD
<table class="table table-hover table-condensed" border="0" cellpadding="20" cellspacing="20" align="center" width="100%">
<thead>
    <tr nobr="true">
        <th width="400px"><strong>System name</strong></th>
        <th>Location (hostname)</th>
        <th width="300px">Events last 30 days</th>
        <th width="300px">Processors avg.</th>
        <th width="300px">Physical memory used</th>
        <th width="300px">Disk usage</th>
    </tr>
</thead>
EOD;

foreach ($deviceData as $deviceId => $device) {
    $troubled = false;

    if(($device["os"] == $os) || ($os == "all")) {
        $storageCellColor = $thresholdsColors["ok"];
        $storageCellIcon = $thresholdsIcons["ok"];
        foreach($thresholdsStorage as $threshold => $value) {
            if($device["storage_perc"] > $value) {
                $storageCellColor = $thresholdsColors[$threshold];
                $storageCellIcon = $thresholdsIcons[$threshold];
                $troubled = true;
            }
        }

        $memoryCellColor = $thresholdsColors["ok"];
        $memoryCellIcon = $thresholdsIcons["ok"];
        foreach($thresholdsMemory as $threshold => $value) {
            if($device["memory_perc"] > $value) {
                $memoryCellColor = $thresholdsColors[$threshold];
                $memoryCellIcon = $thresholdsIcons[$threshold];
                $troubled = true;
            }
        }

        $processorsCellColor = $thresholdsColors["ok"];
        $processorsCellIcon = $thresholdsIcons["ok"];
        foreach($thresholdsMemory as $threshold => $value) {
            if(round($device["processors_perc_average"]) > $value) {
                $processorsCellColor = $thresholdsColors[$threshold];
                $processorsCellIcon = $thresholdsIcons[$threshold];
                $troubled = true;
            }
        }

        if($device["alerts_open"] > 0) {
            $alertsWarning = '<i class="fa fa-bell" aria-hidden="true" title="This device currently has an open alert"></i>';
        } else {
            $alertsWarning = '';
        }

        if(($troubledOnly == false) || ($troubledOnly == true && $troubled == true)) {
            $table .= '
                <tr nobr="true">
                <td><a href="device/device='.$device["id"].'">'.$device["sysname"].'</a></td>
                <td>'.$device["location"].' ('.$device["hostname"].')</td>
                <td>'.$device["events_count"].' '.$alertsWarning.'</td>
                <td>'.round($device["processors_perc_average"]).'% <font color="'.$processorsCellColor.'"><i class="fa fa-'.$processorsCellIcon.'" aria-hidden="true"></i></font></td>
                <td>'.$device["memory_perc"].'% <font color="'.$memoryCellColor.'"><i class="fa fa-'.$memoryCellIcon.'" aria-hidden="true"></i></font></td>
                <td>'.$device["storage_perc"].'% <font color="'.$storageCellColor.'"><i class="fa fa-'.$storageCellIcon.'" aria-hidden="true"></i></font></td>
                </tr>
                ';
        } else {
            $table .= "";
        }
    }
}

$table .= <<<EOD
</table>
EOD;

echo $table;

?>
